import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class WorkService {
  
  workTime$: Subject<number> = new ReplaySubject<number>(5);
  
  constructor() { }

  getWorkTime(): Observable<number> {
    return this.workTime$.asObservable();
  }
  
  setWorkTime(hours: number): void {
    this.workTime$.next(hours); 
  }
}
