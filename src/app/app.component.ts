import { Component, OnInit } from '@angular/core';

import { WorkService } from './services/work.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  workTimeChanges: number[] =[];
  
  constructor(private _workService: WorkService) { }

  ngOnInit() {
    this._workService.setWorkTime(0);
  }
  
  onChangeTimeClick(newTime: number): void {
    this._workService.setWorkTime(newTime); 
  }
  
  onShowChangesClick(): void {
    let workTimeChanges = [];
    this._workService.getWorkTime().subscribe(timeFromResponse => {
      workTimeChanges.push(timeFromResponse);      
    });
    this.workTimeChanges = workTimeChanges;
  }
  
  
}
