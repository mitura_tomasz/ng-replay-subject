import { Component, OnInit } from '@angular/core';
import { WorkService } from './../../services/work.service';

@Component({
  selector: 'app-boss',
  templateUrl: './boss.component.html',
  styleUrls: ['./boss.component.css']
})
export class BossComponent implements OnInit {
  
  workTime: number;
  
  constructor(private _workService: WorkService) { }

  ngOnInit() {
    this._workService.getWorkTime().subscribe(timeFromResponse => {
      this.workTime = timeFromResponse;
    });
  }
}
