import { Component, OnInit } from '@angular/core';
import { WorkService } from './../../services/work.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  workTime: number;
  
  constructor(private _workService: WorkService) { }

  ngOnInit() {
    this._workService.getWorkTime().subscribe(timeFromResponse => {
      this.workTime = timeFromResponse;
    });
  }
  
  onShowChangesClick(): void {
    this._workService.getWorkTime().subscribe(timeFromResponse => {
      console.log(timeFromResponse);
    });
  }
}
