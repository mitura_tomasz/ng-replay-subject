import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BossComponent } from './components/boss/boss.component';
import { EmployeeComponent } from './components/employee/employee.component';

import { WorkService } from './services/work.service';

@NgModule({
  declarations: [
    AppComponent,
    BossComponent,
    EmployeeComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [WorkService],
  bootstrap: [AppComponent]
})
export class AppModule { }
